<?php

use Illuminate\Support\Facades\Route;

// link the PostController file
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// defined a route wherein a view(from) to create a post will be return
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein form data will be sent via a POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all the posts
Route::get('/posts', [PostController::class, 'index']);

// Activity for s02
// define a route that will return a view for the welcome page. 
Route::get('/', [PostController::class, 'welcome']);

Route::get('/myPosts', [PostController::class, 'myPosts']);

Route::get('/posts/{id}', [PostController::class, 'show']);

Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

Route::put('/posts/{id}', [PostController::class, 'update']);